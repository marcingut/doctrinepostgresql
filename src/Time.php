<?php

declare(strict_types=1);

namespace MG\Doctrine;

use DateTime;

class Time extends DateTime
{
    public const FORMAT = 'H:i:s';

    public function __toString(): string
    {
        return $this->format(self::FORMAT);
    }
}
