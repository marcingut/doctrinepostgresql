<?php

declare(strict_types=1);

namespace MG\Doctrine\Types;

use Doctrine\DBAL\Types\IntegerType;

class IntegerRangeType extends AbstractRangesType
{
    public string $type = TypesEnum::INTEGER_RANGE;
    public string $typeClass = IntegerType::class;
}
