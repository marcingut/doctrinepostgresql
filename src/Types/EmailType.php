<?php

declare(strict_types=1);

namespace MG\Doctrine\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use InvalidArgumentException;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Validation;
use Throwable;

use function count;
use function sprintf;

class EmailType extends Type
{
    public function getName(): string
    {
        return TypesEnum::EMAIL;
    }

    /**
     * {@inheritDoc}
     */
    public function getSQLDeclaration(array $column, AbstractPlatform $platform): string
    {
        return $platform->getDoctrineTypeMapping(TypesEnum::EMAIL);
    }

    /**
     * {@inheritDoc}
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ($value === null) {
            return null;
        }

        try {
            if (
                count(Validation::createValidator()->validate(
                    $value,
                    [new Email(['mode' => Email::VALIDATION_MODE_HTML5])],
                )) === 0
            ) {
                return $value;
            }
        } catch (Throwable) {
        }

        throw new InvalidArgumentException(sprintf('%s is not a properly formatted email address type.', $value));
    }
}
