<?php

declare(strict_types=1);

namespace MG\Doctrine\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\IntegerType;

use function is_array;

class IntegerArrayType extends AbstractPgArrayType
{
    public function getName(): string
    {
        return TypesEnum::INTEGER_ARRAY;
    }

    /**
     * {@inheritDoc}
     */
    public function getSQLDeclaration(array $column, AbstractPlatform $platform): string
    {
        return $platform->getDoctrineTypeMapping(TypesEnum::INTEGER_ARRAY);
    }

    /**
     * {@inheritDoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === null) {
            return null;
        }

        return $this->fromPGArray($value);
    }

    /**
     * {@inheritDoc}
     */
    public function convertToDatabaseValue($values, AbstractPlatform $platform)
    {
        if ($values === null) {
            return null;
        }

        if (! is_array($values)) {
            $values = [$values];
        }

        $inet = new IntegerType();

        foreach ($values as &$value) {
            $value = $inet->convertToDatabaseValue($value, $platform);
        }

        return $this->toPGArray($values);
    }
}
