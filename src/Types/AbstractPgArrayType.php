<?php

declare(strict_types=1);

namespace MG\Doctrine\Types;

use Doctrine\DBAL\Types\Type;

use function implode;
use function is_string;
use function str_replace;
use function strlen;
use function substr;

abstract class AbstractPgArrayType extends Type
{
    /** @return string[]|null */
    protected function fromPGArray(string $s, int $start = 0, int|null &$end = null): array|null
    {
        if (empty($s) || $s[0] !== '{') {
            return null;
        }

        $return = [];
        $string = false;
        $quote = '';
        $len = strlen($s);
        $v = '';
        for ($i = $start + 1; $i < $len; $i++) {
            $ch = $s[$i];

            if (! $string && $ch === '}') {
                if ($v !== '' || ! empty($return)) {
                    $return[] = $v;
                }

                $end = $i;

                break;
            } elseif (! $string && $ch === '{') {
                $v = $this->fromPGArray($s, $i, $i);
            } elseif (! $string && $ch === ',') {
                $return[] = $v;
                $v = '';
            } elseif (! $string && ($ch === '"' || $ch === "'")) {
                $string = true;
                $quote = $ch;
            } elseif ($string && $ch === $quote && $s[$i - 1] === '\\') {
                $v = substr($v, 0, -1) . $ch;
            } elseif ($string && $ch === $quote && $s[$i - 1] !== '\\') {
                $string = false;
            } else {
                $v .= $ch;
            }
        }

        return $return;
    }

    /** @param string[]|null $array */
    protected function toPGArray(array|null $array): string
    {
        if ($array === null) {
            return 'null';
        }

        foreach ($array as &$value) {
            if ($value === null) {
                $value = 'NULL';
            } elseif (is_string($value)) {
                $value = '"' . str_replace('"', '\"', $value) . '"';
            }
        }

        return '{' . implode(',', $array) . '}';
    }
}
