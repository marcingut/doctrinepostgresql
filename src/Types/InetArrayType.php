<?php

declare(strict_types=1);

namespace MG\Doctrine\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;

use function is_array;

class InetArrayType extends AbstractPgArrayType
{
    public function getName(): string
    {
        return TypesEnum::INET_ARRAY;
    }

    /**
     * {@inheritDoc}
     */
    public function getSQLDeclaration(array $column, AbstractPlatform $platform): string
    {
        return $platform->getDoctrineTypeMapping(TypesEnum::INET_ARRAY);
    }

    /**
     * {@inheritDoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === null) {
            return null;
        }

        return $this->fromPGArray($value);
    }

    /**
     * {@inheritDoc}
     */
    public function convertToDatabaseValue($values, AbstractPlatform $platform)
    {
        if ($values === null) {
            return null;
        }

        if (! is_array($values)) {
            $values = [$values];
        }

        $inet = new InetType();

        foreach ($values as &$value) {
            $value = $inet->convertToDatabaseValue($value, $platform);
        }

        return $this->toPGArray($values);
    }
}
