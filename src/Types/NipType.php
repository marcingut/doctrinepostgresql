<?php

declare(strict_types=1);

namespace MG\Doctrine\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use InvalidArgumentException;
use Kiczort\PolishValidator\NipValidator;
use Throwable;

use function sprintf;

class NipType extends Type
{
    public function getName(): string
    {
        return TypesEnum::NIP;
    }

    /**
     * {@inheritDoc}
     */
    public function getSQLDeclaration(array $column, AbstractPlatform $platform): string
    {
        return $platform->getDoctrineTypeMapping(TypesEnum::NIP);
    }

    /**
     * {@inheritDoc}
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ($value === null) {
            return null;
        }

        try {
            if ((new NipValidator())->isValid($value)) {
                return $value;
            }
        } catch (Throwable) {
        }

        throw new InvalidArgumentException(sprintf('%s is not a properly formatted NIP type.', $value));
    }
}
