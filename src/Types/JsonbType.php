<?php

declare(strict_types=1);

namespace MG\Doctrine\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\JsonType;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

use function is_object;

class JsonbType extends JsonType
{
    public const JSON_CLASS_KEY = '_class';
    public const JSON_CLASS_VALUE = '_value';

    public function getName(): string
    {
        return TypesEnum::JSONB;
    }

    /**
     * {@inheritDoc}
     */
    public function getSQLDeclaration(array $column, AbstractPlatform $platform): string
    {
        $column['jsonb'] = true;

        return parent::getSQLDeclaration($column, $platform);
    }

    /**
     * {@inheritDoc}
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        $newValue = $value;

        if (is_object($value)) {
            $newValue = [];
            $newValue[self::JSON_CLASS_KEY] = $value::class;
            $newValue[self::JSON_CLASS_VALUE] = $value;
        }

        return parent::convertToDatabaseValue($newValue, $platform);
    }

    /**
     * {@inheritDoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        $result = parent::convertToPHPValue($value, $platform);

        if (! empty($result[self::JSON_CLASS_KEY] ?? null)) {
            $normalizers = [
                new ObjectNormalizer(null, null, null, new ReflectionExtractor()),
                new ArrayDenormalizer(),
            ];

            $serializer = new Serializer($normalizers);

            $result = $serializer->denormalize($result[self::JSON_CLASS_VALUE] ?? null, $result[self::JSON_CLASS_KEY], null, [AbstractObjectNormalizer::DEEP_OBJECT_TO_POPULATE => true]);
        }

        return $result;
    }
}
