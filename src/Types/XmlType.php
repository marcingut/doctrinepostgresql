<?php

declare(strict_types=1);

namespace MG\Doctrine\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\TextType;
use InvalidArgumentException;
use SimpleXMLElement;
use Throwable;

use function is_string;
use function libxml_use_internal_errors;
use function sprintf;

class XmlType extends TextType
{
    public function getName(): string
    {
        return TypesEnum::XML;
    }

    /**
     * {@inheritDoc}
     */
    public function getSQLDeclaration(array $column, AbstractPlatform $platform): string
    {
        return $platform->getDoctrineTypeMapping(TypesEnum::XML);
    }

    /**
     * {@inheritDoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === null) {
            return null;
        }

        try {
            $result = new SimpleXMLElement($value);
        } catch (Throwable) {
            $result = null;
        }

        return $result;
    }

    /**
     * {@inheritDoc}
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ($value === null) {
            return null;
        }

        $result = null;

        if (is_string($value)) {
            libxml_use_internal_errors(true);

            try {
                $value = new SimpleXMLElement($value);
            } catch (Throwable) {
                //throw ConversionException::conversionFailedInvalidType($value, $this->getName(), ['null', 'XML']);
                throw new InvalidArgumentException(sprintf('%s is not a properly formatted XML type.', $value));
            }
        }

        if ($value instanceof SimpleXMLElement) {
            $result = $value->asXML();
        }

        return $result;
    }
}
