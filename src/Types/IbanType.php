<?php

declare(strict_types=1);

namespace MG\Doctrine\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use InvalidArgumentException;
use Symfony\Component\Validator\Constraints\Iban;
use Symfony\Component\Validator\Validation;
use Throwable;

use function count;
use function sprintf;

class IbanType extends Type
{
    public function getName(): string
    {
        return TypesEnum::IBAN;
    }

    /**
     * {@inheritDoc}
     */
    public function getSQLDeclaration(array $column, AbstractPlatform $platform): string
    {
        return $platform->getDoctrineTypeMapping(TypesEnum::IBAN);
    }

    /**
     * {@inheritDoc}
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ($value === null) {
            return null;
        }

        try {
            if (count(Validation::createValidator()->validate($value, [new Iban()])) === 0) {
                return $value;
            }
        } catch (Throwable) {
        }

        throw new InvalidArgumentException(sprintf('%s is not a properly formatted IBAN type.', $value));
    }
}
