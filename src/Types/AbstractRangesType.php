<?php

declare(strict_types=1);

namespace MG\Doctrine\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\Type;

use function assert;
use function preg_match;

abstract class AbstractRangesType extends Type
{
    public string $type;
    public string $typeClass;

    /**
     * {@inheritDoc}
     */
    public function getSQLDeclaration(array $column, AbstractPlatform $platform): string
    {
        return $platform->getDoctrineTypeMapping($this->type);
    }

    public function getName(): string
    {
        return $this->type;
    }

    /**
     * {@inheritDoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === null) {
            return null;
        }

        $range = new RangeType();
        $type = new $this->typeClass();
        assert($type instanceof Type);

        $arr = [];
        if (preg_match('/^(?<bonund_left>[(\[])(?<start>[^,]+),(?<stop>.+)(?<bonund_right>[)\]])$/', $value, $arr)) {
            $range->setStart($type->convertToPHPValue($arr['start'], $platform));
            $range->setStop($type->convertToPHPValue($arr['stop'], $platform));
            $range->setBounds($arr['bonund_left'] . $arr['bonund_right']);
        }

        return $range;
    }

    /**
     * {@inheritDoc}
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): string|null
    {
        if ($value === null) {
            return null;
        }

        $type = new $this->typeClass();
        assert($type instanceof Type);

        if ($value instanceof RangeType) {
            $bounds = $value->getBounds();

            return $bounds[0] .
                $type->convertToDatabaseValue($value->getStart(), $platform) .
                ',' .
                $type->convertToDatabaseValue($value->getStop(), $platform) .
                $bounds[1];
        }

        throw ConversionException::conversionFailedInvalidType($value, $this->getName(), ['null', 'RangeType']);
    }
}
