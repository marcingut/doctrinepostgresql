<?php

declare(strict_types=1);

namespace MG\Doctrine\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\DateType as DoctrineDateType;
use MG\Doctrine\DateTime;

class DateType extends DoctrineDateType
{
    /**
     * {@inheritDoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === null) {
            return $value;
        }

        $res = parent::convertToPHPValue($value, $platform);

        return new DateTime($res->format(DateTime::RFC3339));
    }
}
