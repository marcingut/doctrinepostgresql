<?php

declare(strict_types=1);

namespace MG\Doctrine\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use InvalidArgumentException;
use Kiczort\PolishValidator\PeselValidator;
use Throwable;

use function sprintf;

class PeselType extends Type
{
    public function getName(): string
    {
        return TypesEnum::PESEL;
    }

    /**
     * {@inheritDoc}
     */
    public function getSQLDeclaration(array $column, AbstractPlatform $platform): string
    {
        return $platform->getDoctrineTypeMapping(TypesEnum::PESEL);
    }

    /**
     * {@inheritDoc}
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ($value === null) {
            return null;
        }

        try {
            if ((new PeselValidator())->isValid($value, ['strict' => true])) {
                return $value;
            }
        } catch (Throwable) {
        }

        throw new InvalidArgumentException(sprintf('%s is not a properly formatted PESEL type.', $value));
    }
}
