<?php

declare(strict_types=1);

namespace MG\Doctrine\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\TextType;

use function iconv;
use function mb_strlen;
use function mb_substr;
use function ord;
use function preg_replace;
use function strtolower;
use function trim;

class SysNameType extends TextType
{
    public function getName(): string
    {
        return TypesEnum::SYS_NAME;
    }

    /**
     * {@inheritDoc}
     */
    public function getSQLDeclaration(array $column, AbstractPlatform $platform): string
    {
        return $platform->getDoctrineTypeMapping(TypesEnum::SYS_NAME);
    }

    /**
     * {@inheritDoc}
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ($value === null) {
            return null;
        }

        $value = trim($value);
        $result = '';
        $encoding = 'UTF-8';

        for ($i = 0,$n = mb_strlen($value, $encoding); $i < $n; $i++) {
            $oneChar = mb_substr($value, $i, 1, $encoding);
            if (ord($oneChar) !== 63) {
                $strTmp = iconv($encoding, 'US-ASCII//TRANSLIT', $oneChar);
                if (ord($strTmp) === 63) {
                    $strTmp = '';
                }
            } else {
                $strTmp = $oneChar;
            }

            $result .= $strTmp;
        }

        $result = strtolower($result);
        $result = preg_replace('/[ -]+/U', '_', $result);
        $result = preg_replace('/[^a-z0-9_]+/', '', $result);

        return $result;
    }
}
