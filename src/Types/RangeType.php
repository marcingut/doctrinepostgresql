<?php

declare(strict_types=1);

namespace MG\Doctrine\Types;

class RangeType
{
    protected mixed $start;
    protected mixed $stop;
    protected string $bounds = '[]';

    public function getStart(): mixed
    {
        return $this->start;
    }

    public function setStart(mixed $start): RangeType
    {
        $this->start = $start;

        return $this;
    }

    public function getStop(): mixed
    {
        return $this->stop;
    }

    public function setStop(mixed $stop): RangeType
    {
        $this->stop = $stop;

        return $this;
    }

    public function getBounds(): string
    {
        return $this->bounds;
    }

    public function setBounds(string $bounds): RangeType
    {
        $this->bounds = $bounds;

        return $this;
    }
}
