<?php

declare(strict_types=1);

namespace MG\Doctrine\Types;

use Doctrine\DBAL\Types\Types as DoctrineTypes;

enum TypesEnum: string
{
    public const DATETIME_MICROSEC = DoctrineTypes::DATETIME_MUTABLE;
    public const EMAIL = 'email';
    public const IBAN = 'iban';
    public const INET = 'inet';
    public const JSONB = 'jsonb';
    public const NIP = 'nip';
    public const PESEL = 'pesel';
    public const REGON = 'regon';
    public const SYS_NAME = 'sys_name';
    public const TEXT_ARRAY = '_text';
    public const INET_ARRAY = '_inet';
    public const INTEGER_ARRAY = '_int';
    public const XML = 'xml';
    public const INTEGER_RANGE = 'int4range';
    public const INTERVAL = 'interval';
}
