<?php

declare(strict_types=1);

namespace MG\Doctrine\Types;

use DateTime;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\TimeType as DoctrineTimeType;
use MG\Doctrine\Time;

class TimeType extends DoctrineTimeType
{
    /**
     * {@inheritDoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === null) {
            return $value;
        }

        $res = parent::convertToPHPValue($value, $platform);

        return new Time($res->format(DateTime::RFC3339));
    }
}
