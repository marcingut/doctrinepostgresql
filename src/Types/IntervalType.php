<?php

declare(strict_types=1);

namespace MG\Doctrine\Types;

use DateInterval as Interval;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\Type;
use InvalidArgumentException;

use function preg_match;
use function sprintf;
use function trim;

class IntervalType extends Type
{
    public function getName(): string
    {
        return TypesEnum::INTERVAL;
    }

    /**
     * {@inheritDoc}
     */
    public function getSQLDeclaration(array $column, AbstractPlatform $platform): string
    {
        return $platform->getDoctrineTypeMapping(TypesEnum::INTERVAL);
    }

    /**
     * {@inheritDoc}
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ($value === null) {
            return null;
        }

        if (! $value instanceof Interval) {
            throw new InvalidArgumentException('Interval value must be instance of DateInterval');
        }

        $parts = [
            'y' => 'year',
            'm' => 'month',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        ];

        $sql = '';
        foreach ($parts as $key => $part) {
            $val = $value->{$key};
            if (empty($val)) {
                continue;
            }

            $sql .= sprintf(' %s %s', $val, $part);
        }

        return trim($sql);
    }

    /**
     * {@inheritDoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === null) {
            return null;
        }

        $matches = [];
        preg_match(
            '/^(?:(?P<y>\d+) (?:year|years))?'
            . ' ?(?:(?P<m>\d+) (?:months|month|mons|mon))?'
            . ' ?(?:(?P<d>\d+) (?:days|day))?'
            . ' ?(?:(?P<h>\d{2}):(?P<i>\d{2}):(?P<s>\d{2}))?$/i',
            $value,
            $matches,
        );

        if (empty($matches)) {
            throw ConversionException::conversionFailed($value, self::NAME);
        }

        $interval = new Interval('PT0S');

        if (! empty($matches['y'])) {
            $interval->y = $matches['y'];
        }

        if (! empty($matches['m'])) {
            $interval->m = $matches['m'];
        }

        if (! empty($matches['d'])) {
            $interval->d = $matches['d'];
        }

        if (! empty($matches['h'])) {
            $interval->h = $matches['h'];
        }

        if (! empty($matches['i'])) {
            $interval->i = $matches['i'];
        }

        if (! empty($matches['s'])) {
            $interval->s = $matches['s'];
        }

        return $interval;
    }
}
