<?php

declare(strict_types=1);

namespace MG\Doctrine\Functions;

/**
 * JsonbAppendFunction ::= "jsonb_append" "(" ArithmeticPrimary " ORDER BY " ArithmeticPrimary Identifier ")"
 */
class Count extends AggregateOneParameterFiltered
{
}
