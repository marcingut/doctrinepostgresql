<?php

declare(strict_types=1);

namespace MG\Doctrine\Functions;

use Doctrine\ORM\Query\AST\Node;
use Doctrine\ORM\Query\AST\OrderByClause;
use Doctrine\ORM\Query\AST\PathExpression;
use Doctrine\ORM\Query\AST\SimpleArithmeticExpression;

class AggregateExpression extends \Doctrine\ORM\Query\AST\AggregateExpression
{
    public function __construct(
        string $functionName,
        Node $pathExpression,
        bool $isDistinct,
        public OrderByClause|PathExpression|SimpleArithmeticExpression|null $orderByExpression = null,
    ) {
        parent::__construct($functionName, $pathExpression, $isDistinct);
    }

    /**
     * {@inheritDoc}
     */
    public function dispatch($walker)
    {
        $sql = $this->functionName . '(' . ($this->isDistinct ? 'DISTINCT ' : '')
            . $walker->walkSimpleArithmeticExpression($this->pathExpression);
//        $sql = parent::dispatch($walker);

        if ($this->orderByExpression !== null) {
            $sql .= ' ' . $this->orderByExpression->dispatch($walker);
        }

        $sql .= ')';

        return $sql;
    }
}
