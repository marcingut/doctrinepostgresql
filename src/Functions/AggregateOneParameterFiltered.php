<?php

declare(strict_types=1);

namespace MG\Doctrine\Functions;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\AST\WhereClause;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;
use Doctrine\ORM\Query\TokenType;

use function strtolower;

class AggregateOneParameterFiltered extends FunctionNode
{
    private AggregateExpression|null $aggregate = null;
    private WhereClause|null $filterWhereType = null;

    public function parse(Parser $parser): void
    {
        $lexer = $parser->getLexer();
        $lookaheadType = $lexer->lookahead->type;
        $isDistinct = false;

        $parser->match($lookaheadType);
        $functionName = $lexer->token?->value;
        $parser->match(TokenType::T_OPEN_PARENTHESIS);

        if ($lexer->isNextToken(TokenType::T_DISTINCT)) {
            $parser->match(TokenType::T_DISTINCT);
            $isDistinct = true;
        }

        $pathExp = $parser->SimpleArithmeticExpression();

        $orderByExp = null;
        if ($lexer->isNextToken(TokenType::T_ORDER)) {
            $orderByExp = $parser->OrderByClause();
        }

        $parser->match(TokenType::T_CLOSE_PARENTHESIS);

        $this->aggregate = new AggregateExpression($functionName, $pathExp, $isDistinct, $orderByExp);

        $lookahead = $lexer->lookahead?->value;

        if (! $lexer->isNextToken(TokenType::T_IDENTIFIER)) {
            return;
        }

        if (strtolower($lookahead) !== 'filter') {
            return;
        }

        $parser->match(TokenType::T_IDENTIFIER); // (2)
        $parser->match(TokenType::T_OPEN_PARENTHESIS); // (3)
        $this->filterWhereType = $parser->WhereClause();
        $parser->match(TokenType::T_CLOSE_PARENTHESIS); // (3)
    }

    public function getSql(SqlWalker $sqlWalker): string
    {
        $sql = $this->aggregate->dispatch($sqlWalker);

        if ($this->filterWhereType !== null) {
            $sql .= ' FILTER (' .
                $this->filterWhereType->dispatch($sqlWalker) .
                ')';
        }

        return $sql;
    }
}
