<?php

declare(strict_types=1);

namespace MG\Doctrine\Functions;

/**
 * SumFunction ::= "sum" "(" ArithmeticPrimary " ORDER BY " ArithmeticPrimary Identifier ") Filter (Where " ")"
 */
class Sum extends AggregateOneParameterFiltered
{
}
