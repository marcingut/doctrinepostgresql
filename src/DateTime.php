<?php

declare(strict_types=1);

namespace MG\Doctrine;

class DateTime extends \DateTime
{
    public function __toString(): string
    {
        return $this->format(self::RFC3339);
    }
}
