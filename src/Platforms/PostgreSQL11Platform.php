<?php

declare(strict_types=1);

namespace MG\Doctrine\Platforms;

use Doctrine\DBAL\Platforms\PostgreSQLPlatform;

use function is_string;
use function preg_match;
use function strlen;
use function substr;

class PostgreSQL11Platform extends PostgreSQLPlatform
{
    public const IDENTITY_SEQUENCE_NAME_LIMIT = 63;
/*    public function getListTableColumnsSQL($table, $database = null)
    {
        $sql = parent::getListTableColumnsSQL($table, $database);
//echo $sql;

        $sql = "SELECT
            x.attnum,
            x.field,
          --  (CASE WHEN x.complete_type = 'timestamp without time zone' THEN 'timestamp_microseconds' ELSE x.type END) AS type,
            x.type,
            x.complete_type,
            x.domain_type,
            x.domain_complete_type,
            x.isnotnull,
            x.pri,
            x.default,
            x.comment
        FROM (" .$sql.") x";

        return $sql;
    }*/

    private function isFunction(array $field): bool
    {
        return isset($field['type'], $field['default']) && is_string($field['default'])
            && preg_match('/\w+(?=\().+\)/', $field['default']);
    }

    /**
     * {@inheritDoc}
     */
    public function getDefaultValueDeclarationSQL($column)
    {
        if ($this->isFunction($column)) {
            return ' DEFAULT ' . $column['default'];
        }

        return parent::getDefaultValueDeclarationSQL($column);
    }

    /**
     * {@inheritDoc}
     */
    public function getIdentitySequenceName($tableName, $columnName)
    {
        $limit = self::IDENTITY_SEQUENCE_NAME_LIMIT - 5;// 5 - '_'(1) '_seq'(4)

        if (strlen($tableName) > $limit / 2) {
            $tableName = substr($tableName, 0, $limit / 2);
        }

        if (strlen($tableName) + strlen($columnName) > $limit) {
            $columnName = substr($columnName, 0, $limit - strlen($tableName));
        }

        return parent::getIdentitySequenceName($tableName, $columnName);
    }
}
