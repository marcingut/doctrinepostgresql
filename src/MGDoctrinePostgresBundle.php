<?php

declare(strict_types=1);

namespace MG\Doctrine;

use MG\Doctrine\DependencyInjection\Compiler\BeforeOptimizationDbalPass;
use MG\Doctrine\DependencyInjection\Compiler\BeforeOptimizationOrmPass;
use Symfony\Component\DependencyInjection\Compiler\PassConfig;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class MGDoctrinePostgresBundle extends Bundle
{
    public function build(ContainerBuilder $container): void
    {
        $container->addCompilerPass(new BeforeOptimizationDbalPass(), PassConfig::TYPE_BEFORE_OPTIMIZATION);
        $container->addCompilerPass(new BeforeOptimizationOrmPass(), PassConfig::TYPE_BEFORE_OPTIMIZATION);
    }
}
