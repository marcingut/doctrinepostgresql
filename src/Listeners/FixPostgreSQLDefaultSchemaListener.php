<?php

declare(strict_types=1);

namespace MG\Doctrine\Listeners;

use Doctrine\DBAL\Schema\PostgreSqlSchemaManager;
use Doctrine\ORM\Tools\Event\GenerateSchemaEventArgs;

class FixPostgreSQLDefaultSchemaListener
{
    public function postGenerateSchema(GenerateSchemaEventArgs $args): void
    {
        $connection = $args
            ->getEntityManager()
            ->getConnection();

        $schemaManager = $connection->createSchemaManager();

        if (! $schemaManager instanceof PostgreSqlSchemaManager) {
            return;
        }

        foreach ($schemaManager->getExistingSchemaSearchPaths() as $namespace) {
            if ($args->getSchema()->hasNamespace($namespace)) {
                continue;
            }

            $args->getSchema()->createNamespace($namespace);
        }
    }
}
