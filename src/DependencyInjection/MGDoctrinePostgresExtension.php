<?php

declare(strict_types=1);

namespace MG\Doctrine\DependencyInjection;

use Doctrine\Common\Collections\ArrayCollection;
use MG\Doctrine\Types\TypesEnum;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;

use function dirname;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class MGDoctrinePostgresExtension extends Extension implements PrependExtensionInterface
{
    /** @param array<array<mixed>> $configs */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $loader = new XmlFileLoader($container, new FileLocator(dirname(__DIR__) . '/Resources/config'));
        $loader->load('services.xml');
    }

    public function prepend(ContainerBuilder $container): void
    {
        $doctrineConfig = new ArrayCollection();
        $this->setDbalPostgresTypes($doctrineConfig);
        $container->prependExtensionConfig('doctrine', ['dbal' => $doctrineConfig->toArray()]);
    }

    private function setDbalPostgresTypes(ArrayCollection $doctrineConfig): void
    {
        $config = [
            TypesEnum::XML => TypesEnum::XML,
            TypesEnum::SYS_NAME => TypesEnum::SYS_NAME,
            TypesEnum::INET => TypesEnum::INET,
            TypesEnum::IBAN => TypesEnum::IBAN,
            TypesEnum::EMAIL => TypesEnum::EMAIL,
            TypesEnum::PESEL => TypesEnum::PESEL,
            TypesEnum::NIP => TypesEnum::NIP,
            TypesEnum::REGON => TypesEnum::REGON,
            TypesEnum::JSONB => TypesEnum::JSONB,
            TypesEnum::INET_ARRAY => TypesEnum::INET_ARRAY,
            TypesEnum::INTEGER_ARRAY => TypesEnum::INTEGER_ARRAY,
            TypesEnum::TEXT_ARRAY => TypesEnum::TEXT_ARRAY,
            TypesEnum::INTEGER_RANGE => TypesEnum::INTEGER_RANGE,
            TypesEnum::INTERVAL => TypesEnum::INTERVAL,
        ];

        $doctrineConfig->set('mapping_types', $config);
        $doctrineConfig->set('schema_filter', '~^(?!view_)~');
        $doctrineConfig->set('platform_service', 'PostgreSQL11Platform');
    }
}
