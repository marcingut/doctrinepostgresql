<?php

declare(strict_types=1);

namespace MG\Doctrine\DependencyInjection\Compiler;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Types\Types as DoctrineTypes;
use MG\Doctrine\Types;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

final class BeforeOptimizationDbalPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        $arr = [
            DoctrineTypes::DATE_MUTABLE => Types\DateType::class,
            DoctrineTypes::TIME_MUTABLE => Types\TimeType::class,
            DoctrineTypes::BIGINT => Types\BigIntType::class,
            Types\TypesEnum::DATETIME_MICROSEC => Types\DateTimeWithMicrosecondsType::class,
            Types\TypesEnum::SYS_NAME => Types\SysNameType::class,
            Types\TypesEnum::XML => Types\XmlType::class,
            Types\TypesEnum::JSONB => Types\JsonbType::class,
            Types\TypesEnum::INET => Types\InetType::class,
            Types\TypesEnum::IBAN => Types\IbanType::class,
            Types\TypesEnum::EMAIL => Types\EmailType::class,
            Types\TypesEnum::PESEL => Types\PeselType::class,
            Types\TypesEnum::NIP => Types\NipType::class,
            Types\TypesEnum::REGON => Types\RegonType::class,
            Types\TypesEnum::INET_ARRAY => Types\InetArrayType::class,
            Types\TypesEnum::TEXT_ARRAY => Types\TextArrayType::class,
            Types\TypesEnum::INTEGER_ARRAY => Types\IntegerArrayType::class,
            Types\TypesEnum::INTEGER_RANGE => Types\IntegerRangeType::class,
            Types\TypesEnum::INTERVAL => Types\IntervalType::class,
        ];

        $types = new ArrayCollection($container->getParameter('doctrine.dbal.connection_factory.types'));

        foreach ($arr as $name => $class) {
            $types->set($name, ['class' => $class, 'commented' => false]);
        }

        $container->setParameter('doctrine.dbal.connection_factory.types', $types->toArray());
    }
}
