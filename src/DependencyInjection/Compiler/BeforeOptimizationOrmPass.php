<?php

declare(strict_types=1);

namespace MG\Doctrine\DependencyInjection\Compiler;

use MG\Doctrine\Functions as PostgresFunctions;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;

final class BeforeOptimizationOrmPass implements CompilerPassInterface
{
    private Definition|null $defaultConfiguration = null;

    public function process(ContainerBuilder $container): void
    {
        $this->getDefaultConfigDefinition($container)
            ->addMethodCall('setCustomStringFunctions', [
                [
                    'jsonb_append' => PostgresFunctions\JsonbAppend::class,
                    'array_agg' => PostgresFunctions\ArrayAgg::class,
                ],
            ])
            ->addMethodCall('setCustomNumericFunctions', [
                [
                    'count' => PostgresFunctions\Count::class,
                    'sum' => PostgresFunctions\Sum::class,
                ],
            ]);
    }

    private function getDefaultConfigDefinition(ContainerBuilder $container): Definition
    {
        if ($this->defaultConfiguration === null) {
            $this->defaultConfiguration = $container->getDefinition('doctrine.orm.default_configuration');
        }

        return $this->defaultConfiguration;
    }
}
